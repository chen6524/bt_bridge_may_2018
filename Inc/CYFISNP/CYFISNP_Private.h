//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Private.h
//  Version: 0.99, Updated on 20/07/2013 

//
//  DESCRIPTION: <HUB>Star Network Protocol Private declarations
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CYFISNP_PRIVATE_H
#define CYFISNP_PRIVATE_H

//#include <device.h>

#define CYFISNP_SPI_BITRATE		        0x3
#define CYFISNP_CPU_CYCLES_PER_BYTE   0x20
#define CYFISNP_CPU_CLK               0xc

// ---------------------------------------------------------------------------
// If External PA used, need wider guard band, so increase margins
// ---------------------------------------------------------------------------

#define CYFISNP_CHAN_MIN            0x00
#define CYFISNP_CHAN_MAX            0x4d

#define CYFISNP_CRC_ZERO_SEED       0

#define CYFISNP_PKT_PING_TYPE       (uint8_t)0x00
#define CYFISNP_PKT_PING_LEN        (uint8_t)1
#define CYFISNP_PKT_BEACON_LEN      (uint8_t)4

#define CYFISNP_PKT_data_BCD_CONF   (uint8_t)0x30
#define CYFISNP_PKT_data_BCD_SYNC   (uint8_t)0x32

#define CYFISNP_SIZEOF_MID          4

// ---------------------------------------------------------------------------
// phyIdle() and phyRxGo() are used to access MID Fuses inside the radio
// Some radio operations (like reading the fuses) can not be done while the
//  radio is in recieve (the radio must be "idled", the operation done, and
//  then returned to receive mode, since receive is an ISR operation).
// ---------------------------------------------------------------------------
void CYFISNP_phyIdle         (void);
void CYFISNP_phyRxGo         (void);

// ---------------------------------------------------------------------------
//
// Sequence Bit Structures located on Page 0 (ISR_RAM)
//
// Each consists of an array of CYFISNP_MAX_NODES bits, and there are 3 arrays.
//
// ---------------------------------------------------------------------------
#define REC_OFS     ((CYFISNP_MAX_NODES/8) + 1)
#define CYFISNP_afVsb_ofs   (0 * REC_OFS)   // ValidExpected Sequence Bit array
#define CYFISNP_afEsb_ofs   (1 * REC_OFS)   //      Expected Sequence Bit array
#define CYFISNP_afTsb_ofs   (2 * REC_OFS)   //   Transmitted Sequence Bit array
// --------------------------------------
extern unsigned char CYFISNP_af_BASE[];         // START OF THE 3 ARRAYS.



extern unsigned char CYFISNP_radioTxConfig;     // Copy of _TX_CFG_ADR register

extern unsigned char CYFISNP_hubSeedMsb;
extern unsigned char CYFISNP_hubSeedLsb;


// ---------------------------------------------------------------------------
// CYFISNP_ePhyState - The CURRENT Radio state, whereas "radio end state" is where
//  the radio WILL BE IF it changes state (usually leaving Tx or Rx state).
// ---------------------------------------------------------------------------
typedef enum {
    CYFISNP_PHY_SLEEP = 0,   // Radio is in SLEEP
    CYFISNP_PHY_IDLE  = 1,   // Radio 12MHz running, but no Synthesizer
    CYFISNP_PHY_SYNTH = 2,   // Radio Synthesizer running
    CYFISNP_PHY_RXGO  = 3,   // Radio dedicated to RECEIVE  (Rx pkt or Abort exits)
    CYFISNP_PHY_TXGO  = 4,   // Radio dedicated to TRANSMIT (Tx pkt done exits)
} CYFISNP_PHY_STATE;

extern CYFISNP_PHY_STATE CYFISNP_ePhyState;


// ---------------------------------------------------------------------------
// Assembly routines
// ---------------------------------------------------------------------------

void    CYFISNP_startReceive   (void);
void    CYFISNP_TxHandlerNone  (void);




// ---------------------------------------------------------------------------
//
// Misc test stuff
//
// ---------------------------------------------------------------------------

#define CYFISNP_TEST_DISABLE_RSSI_CHANNEL_CHANGE        0

#endif  // CYFISNP_PRIVATE_H
// ###########################################################################
