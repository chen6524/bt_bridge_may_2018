//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Protocol.h
//  Version: 0.99, Updated on 20/07/2013

//
//  DESCRIPTION: <HUB>Star Network Protocol radio driver Header
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef _CYFI_CYFISNP_H_
#define _CYFI_CYFISNP_H_

#include "cyfi_hal.h"

/*******************************************************************************
 *  CYFISNP_READ_MICROSEC approximates delays in the CYFI driver.  Functions that
 *  need to delay, loop calling CYFISNP_Read().  Since CYFISNP_Read() does 2 SPI 
 *  accesses, the SPI access delay dominates the delay loop iteration time.
 *
 *  Critical internal delays are based on an estimate of execution time of 
 *  CYFISNP_Read().  CYFISNP_Read() produces 16 SPI clocks, so the execution time 
 *  of CYFISNP_Read() is probably dominated by the SPI clock frequency.
 *  If we assume the LP SPI clocks as fast as possible (238 nS), then 16 
 *  SPI clocks takes 3.8 uS and we can say CYFISNP_Read() takes at least 4 uS.
 *  4 uS for 16 SPI clks is fastest possible (very conservative)
 *  10 to 20 uS is more realistic for most platforms.
 *******************************************************************************/
#define CYFISNP_READ_MICROSEC                 20

/*******************************************************************************
 * CYFISNP_FIFO_LEN defines the physical data FIFO lengths in CYFISNP_ 
 ********************************************************************************/
#define CYFISNP_FIFO_LEN                      16

/* CYFISNP_ driver states defined */
#define CYFISNP_IDLE                          0x00
#define CYFISNP_RX                            0x80
#define CYFISNP_TX                            0x20
#define CYFISNP_SOP                           SOFDET_IRQ
#define CYFISNP_data                          RXB1_IRQ
#define CYFISNP_COMPLETE                      RXC_IRQ
#define CYFISNP_ERROR                         RXE_IRQ

#define CYFISNP_ABORT_SUCCESS                 0xFF

 /* Receiver status defined */
#define CYFISNP_RX_ACK                        0x80
#define CYFISNP_RX_MISS                       0x40
#define CYFISNP_RX_OF                         0x20
#define CYFISNP_RX_CRC0                       0x10
#define CYFISNP_BAD_CRC                       0x08
#define CYFISNP_DATCODE_LEN                   0x04
#define CYFISNP_SDR                           0x03
#define CYFISNP_DDR                           0x02
#define CYFISNP_8DR                           0x01
#define CYFISNP_GFSK                          0x00

/* Function Declaration */
void        CYFISNP_Init               (uint8  defaultXactState, uint8  defaultTxState);
void        CYFISNP_StartTransmit      (uint8 retryCount,uint8 length);
uint8      	CYFISNP_BlockingTransmit   (uint8 retryCount,uint8 length);
void        CYFISNP_StartReceive       (void);
uint8      	CYFISNP_GetTransmitState   (void);
uint8      	CYFISNP_GetReceiveState    (void);
uint8 		CYFISNP_GetReceiveStatus   (void);
void        CYFISNP_EndTransmit        (void);
uint8    	CYFISNP_EndReceive         (void);
void        CYFISNP_ForceState         (uint8  endStateBitsOnly);
uint8      	CYFISNP_Poll               (void);
void        CYFISNP_Interrupt          (void);
uint8    	CYFISNP_Abort              (void);
void        CYFISNP_SetFrequency       (uint8 frequency);
void        CYFISNP_SetChannel         (uint8 channel);
uint8   	CYFISNP_GetFrequency       (void);
uint8   	CYFISNP_GetChannel         (void);
void        CYFISNP_SetTxConfig        (uint8  config);
uint8       CYFISNP_GetTxConfig        (void);
void        CYFISNP_SetXactConfig      (uint8  config);
uint8      	CYFISNP_GetXactConfig      (void);
void        CYFISNP_SetTxConfig        (uint8  config);
void        CYFISNP_SetFrameConfig     (uint8 config);
uint8 		CYFISNP_GetFrameConfig  	(void);
void        CYFISNP_SetThreshold32     (uint8 threshold);
uint8   	CYFISNP_GetThreshold32     (void);
void        CYFISNP_SetThreshold64     (uint8 threshold);
uint8   	CYFISNP_GetThreshold64     (void);
void        CYFISNP_SetPreambleCount   (uint8 count);
uint8   	CYFISNP_GetPreambleCount   (void);
void        CYFISNP_SetPreamblePattern (uint16  pattern);
uint16   	CYFISNP_GetPreamblePattern (void);
void        CYFISNP_SetSopPnCode       (uint8 patternNum);
void        CYFISNP_SetConstdataPnCode (const uint8 *patternAddr);
void        CYFISNP_SetConstSopPnCode  (const uint8 *patternAddr);
void        CYFISNP_SetCrcSeed         (uint16  crcSeed);
uint16   	CYFISNP_GetCrcSeed         (void);
void        CYFISNP_ReadFuses          (void);
uint8      	CYFISNP_GetRssi            (void);
void        CYFISNP_GetFuses           (void);
void        CYFISNP_RestartTransmit    (void);
#endif 
