
//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Time.h
//  Version:0.99 , Updated on 20/07/2013
//  
//
//  DESCRIPTION: Star Network Protocol Time routnes header file
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CYFISNP_TIME_H
#define CYFISNP_TIME_H

//#include "typedef.h"
#include "stm32l4xx.h"

extern  __IO uint32_t CYFISNP_SleepTimer_TickCount;

void    CYFISNP_TimeSet     (uint32_t *pTimer, uint32_t time);
uint8   CYFISNP_TimeExpired (uint32_t *pTimer);

#define CYFISNP_TIMER_UNITS     1

#endif  // CYFISNP_TIME_H
// ###########################################################################
//[] END OF FILE
