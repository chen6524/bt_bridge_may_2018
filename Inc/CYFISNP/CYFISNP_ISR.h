
//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Time.h
//  Version:0.99 , Updated on 20/07/2013
//  
//
//  DESCRIPTION: Star Network Protocol Time routnes header file
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CYFISNP_ISR_H
#define CYFISNP_ISR_H

#include "CYFISNP_Config.h"
#include "typedef.h"


typedef struct
{
    uint8   rxIsrBufLen;                            // Rx packet length
    uint8   rxIsrBufRssi;                           // Rx RSSI
    uint8   rxIsrBufProt;                           // Protocol Byte
    uint8   rxIsrBufDev;                            // Device ID
    uint8   rxIsrBufPay[CYFISNP_FCD_PAYLOAD_MAX];   // API payload
}IsrPkt;


uint8 CYFISNP_IRQ_Read(void);
void CYFISNP_ISR_Interrupt(void);

#endif  // CYFISNP_ISR_H
// ###########################################################################
//[] END OF FILE
