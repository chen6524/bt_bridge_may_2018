/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
  * http:  www.artaflex.com

 *
 ***************************************************************************/
#ifndef _MAX14676_H_
#define _MAX14676_H_

//#include "typedef.h"
#include "stm32l4xx.h"

#define MAX14676_DEV_ADDR                       0x50   //device address 0x28 << 1
#define MAX14676_CHIP_ID                        0x00
#define MAX14676_STATUS_A                       0x02
#define MAX14676_STATUS_B                       0x03
#define MAX14676_STATUS_C                       0x04
#define MAX14676_INT_A                          0x05
#define MAX14676_INT_B                          0x06
#define MAX14676_INT_MASKA                      0x07
#define MAX14676_INT_MASKB                      0x08
#define MAX14676_ILIMCNTL_ADDR                  0x0A
#define MAX14676_CHGCNTLA                       0x0B
#define MAX14676_CHGCNTLB                       0x0C
#define MAX14676_CHGVSET                        0x0E
#define MAX14676_PCHGCNTL                       0x10
#define MAX14676_CDETCNTLB                      0x11
#define MAX14676_LDOCFG                         0x16
#define MAX14676_PWR_CFG_ADDR 					        0x1E

#define USBOK_STATUS                            0x08            //(1 << 3)
#define USBOVP_STATUS                           (1 << 5)

#define USBOVP_INT                              (1 << 6)
#define USBOK_INT                               0x08            //(1 << 3)
#define CHGSTAT_INT                             0x04            //(1 << 2)

#define USBOVP_MASK                             (1 << 6)
#define USBOK_MASK                              0x08            //(1 << 3)
#define CHGSTAT_MASK                            0x04            //(1 << 2)

#define CHARGER_OFF                             0x00
#define CHARGEING_SUSPENDED                     0x01
#define PRECHARGE_IN_PROCESS                    0x02
#define CHARGE_CONSTANT_CURRENT                 0x03
#define CHARGE_CONSTANT_VOLTAGE                 0x04
#define CHARGE_IN_PROCESS                       0x05
#define CHARGER_TIMER_DONE                      0x06
#define CHARGER_FAULT_CONDITION                 0x07


#define MAX14676_DEV_ADDR2                      0x6C            //0x36 << 1

#define MAX14676_VALRT                          0x14
#define MAX14676_STATUS 				                0x1A    //0x00

void max14676_init(void);
void max14676_GetVer(void);
void EnableCHG(void);

uint8_t max14676_Read(uint8_t regAddr);
uint8_t max14676_ReadReg(uint8_t regAddr, uint8_t* ptrVal);
uint8_t max14676_ReadReg2(uint16_t * pReadVal, uint8_t regAddr);

void max14676_process(void);

uint8_t max14676_GetBatteryCharge(void);
uint16_t max14676_GetBatteryChargeVoltage( void);
void max14676_poweroff(void);

extern uint8_t max14676Exist;
extern uint8_t max14676ChargeStat;
extern uint16_t max14676Voltage;
extern uint16_t max14676Voltage_old ;                  // 2014.12.08
extern uint8_t max14676Charge ;

extern uint32_t RCC_CSR_reg;

extern void DisableCHG(void);
extern void ReStart_Process(void);

#endif
