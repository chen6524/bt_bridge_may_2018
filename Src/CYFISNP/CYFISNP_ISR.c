//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_ISR.c
//  Version: 1.0, Updated on 20/07/2015

//
//  DESCRIPTION: <HUB>Star Network Protocol Header
//-----------------------------------------------------------------------------
//  Copyright (c) Artaflex INC 2015. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
//#include "device.h"
//#include "CYFISNP_Timer.h"
#include "CYFISNP_ISR.h"

//#include "derivative.h"              /* include peripheral declarations */
#include "CYFISNP_Config.h"
#include "cyfi_radio.h"
#include "CYFISNP_Protocol.h"
//#include "hardware.h"

//#include "mcu.h"

//#pragma MESSAGE DISABLE C1420
#include "gpio.h"
#include <string.h>


#define CYFISNP_BCD_PAYLOAD_MAX     0x01//0x05
#define CYFISNP_SIZEOF_MID          4
#define DEV_REC_devId_OFS           1
#define DEV_REC_devMid_OFS          2 
#define DEV_REC_BCD_DLY_OFS         6
/* ----------------------------------------------------------------------------
 CYFISNP_FCD_PAYLOAD_MAX - Max Forward Channel data Payload length
-----------------------------------------------------------------------------*/
#define CYFISNP_FCD_PAYLOAD_MAX     14

/*-----------------------------------------------------------------------------
; SNP PACKET HEADERS (some also exist in CYFISNP_Config.h)
-----------------------------------------------------------------------------*/

#define PKT_BINDREQ_TYPE            (uint8_t)0x00
#define PKT_BINDREQ_MASK            (uint8_t)0xFC
#define PKT_BIND_REQ_LEN            (uint8_t)7
//-----------------------------------------------------------------------------
#define PKT_CONREQ_TYPE             (uint8_t)0x00
#define PKT_CONREQ_LEN              (uint8_t)6
//-----------------------------------------------------------------------------
#define PKT_BINDRSP_TYPE            (uint8_t)0x10
#define PKT_BINDRSPDEL_TYPE         (uint8_t)0x11
#define PKT_BINDRSP_LEN             (uint8_t)10
//------------------------------------------------------------------------------
#define PKT_CONRSP_TYPE             (uint8_t)0x10
#define PKT_CONRSP_LEN              (uint8_t)6
//------------------------------------------------------------------------------
#define PKT_UNBIND_TYPE             (uint8_t)0x10
#define PKT_UNBIND_LEN              (uint8_t)2
//------------------------------------------------------------------------------
#define PKT_data_TYPE               (uint8_t)0x20
#define PKT_data_BCDR_MASK          (uint8_t)0x04
#define PKT_data_MASK               (uint8_t)0xF0
//-------------------------------------------------------------------------------
#define PKT_SEQBIT_MASK             (uint8_t)0x03
#define PKT_SEQTX_0                 (uint8_t)0x00
#define PKT_SEQTX_1                 (uint8_t)0x01
#define PKT_SEQSYNC                 (uint8_t)2
#define PKT_SEQUNK                  (uint8_t)3
//--------------------------------------------------------------------------------
#define PKT_UNKNOWN_TYPE            (uint8_t)0xFF
//--------------------------------------------------------------------------------

//extern volatile byte        *EEPROM_Pointer;
extern uint8           		  CYFISNP_Len; /* Len of User's Payload buffer */
extern CYFISNP_BCD_PKT      CYFISNP_BcdBufs[CYFISNP_MAX_NODES];
extern uint8                CYFISNP_RetryCount;

extern const uint8         *EEPROM_Pointer;
extern CYFISNP_API_PKT      CYFISNP_RxApiPkt; // Public for ISR (not API)
extern uint8                CYFISNP_State;

/* ----------------------------------------------------------------------------
; Copy of CYFISNP_TX_CFG_ADR register
; ---------------------------------------------------------------------------*/
uint8   CYFISNP_radioTxConfig;

/*----------------------------------------------------------------------------
; CYFISNP_IsrRxOverflow - Indicates Rx Overflow condition.
-----------------------------------------------------------------------------*/
uint8   CYFISNP_IsrRxOverflow;


uint8   CYFISNP_BindProgress;

#define PASSED_BIND_NONE    0
#define PASSED_BIND_REQ     1
#define PASSED_BIND_RSP     2
#define PASSED_BIND_RSP_ACK 3

/*----------------------------------------------------------------------------
;
; rxIsrBuf - Rx ISR buffer (includes length prefix)
;
;----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
; If RSSI Polling gets a SOP reading, it goes in _RssiPolled for RxPkt tagging
;----------------------------------------------------------------------------*/
uint8   CYFISNP_RssiPolled;

uint8   devIdMatch;         // Zero if RxDevId exists in FLASH
uint8   rxStatusVal;        // Radio Rx Status

/*-------------------------------
; Rx Packet buffer copied to API
------------------------------- */
//struct IsrPkt
//{
//    uint8   rxIsrBufLen;                            // Rx packet length
//    uint8   rxIsrBufRssi;                           // Rx RSSI
//    uint8   rxIsrBufProt;                           // Protocol Byte
//    uint8   rxIsrBufDev;                            // Device ID
//    uint8   rxIsrBufPay[CYFISNP_FCD_PAYLOAD_MAX];   // API payload
//};
/*struct*/ IsrPkt rxIsrBufPkt;

/* ----------------------------------------------------------------------------
; ePhyState - Physical radio state, used to keep ISR from returning to RX_GO
;             after receiving a packet when NON-ISR is trying to exit RX_GO.
;             (usually to change channels)
; ----------------------------------------------------------------------------*/
//#define CYFISNP_PHY_SLEEP   0                  // Commented by Jason Chen, 2017.08.29
//#define CYFISNP_PHY_IDLE    1
//#define CYFISNP_PHY_SYNTH   2
//#define CYFISNP_PHY_RXGO    3
//#define CYFISNP_PHY_TXGO    4

CYFISNP_PHY_STATE   CYFISNP_ePhyState;

/*-----------------------------------------------------------------------------

 txIsrBuf - ISR use only, Tx packet buf and working Rx MID comparison buf.

  ISR uses txIsrBuf to compose Response packets.  It also copies the MID from
  received rxIsrBufPkt here to simplify searching FLASH for a matching MID.

---------------------------------------------------------------------------*/
#define RX_MID0         txIsrBuf[2]
#define RX_MID1         txIsrBuf[3]
#define RX_MID2         txIsrBuf[4]
#define RX_MID3         txIsrBuf[5]
#define pPostAndGo      txIsrBuf[0]
#define postAndGoLen    txIsrBuf[1]

uint8   txIsrBuf[10];

/*-----------------------------------------------------------------------------
 Local ISR scratchpad
----------------------------------------------------------------------------- */
uint8   bitMask;
uint8   ivar;
uint8   bcdRspDly;
//----------------------------------------------------------------------------

uint8   temp;  // used in bind table search
//uint8   pDevRecLsb;

/*-----------------------------------------------------------------------------
 CYFISNP_hubSeed - RAM copy of Hub CRC Seed stored in FLASH.
  It's faster and uses less FLASH to burn 2 RAM bytes to hold this
 -----------------------------------------------------------------------------*/

uint8   CYFISNP_hubSeedMsb;
uint8   CYFISNP_hubSeedLsb;


/*----------------------------------------------------------------------------
; txRet vars are used by Transmit ISR "suspend/resume"
 ----------------------------------------------------------------------------*/

#define TX_RET_NONE    0
#define TX_RET_UNBIND  1
#define TX_RET_CONRSP  2
#define TX_RET_BINDRSP 3
#define TX_RET_BCDRSP  4

uint8   txRetVec;
uint8   txRetState;

/* ----------------------------------------------------------------------------
; Arrays of Node RAM Flags:
;       array 1 = (Vsb) Valid Bits (Expected Sequence Bit is Valid)
;       array 2 = (Esb) Expected Sequence Bits
;       array 3 = (Tsb) Transmit Sequence Bits
; ----------------------------------------------------------------------------*/
#define SB_BYTE_SZ   (1+(CYFISNP_MAX_NODES/8))               // = 1+15
#define afVsb_ofs    (0*SB_BYTE_SZ)                          // = 0           ValidExpected Sequence Bit array
#define afEsb_ofs    (1*SB_BYTE_SZ)                          // = 16          Expected Sequence      Bit array
#define afTsb_ofs    (2*SB_BYTE_SZ)                          // = 32          Transmitted Sequence   Bit array
uint8   CYFISNP_af_BASE[3*SB_BYTE_SZ];//3 * SB_BYTE_SZ];     // = 48          //

/* ----------------------------------------------------------------------------
;
; postAndRxGo - Post Rx packet for Non Interrupt handling
;
------------------------------------------------------------------------------*/
#define RF_RING_BUF_NUM                   16    // 16 additional buffers for ISR RX
struct {							                    
	uint8_t         num;
	uint8_t			    inp;
	uint8_t			    outp;
	CYFISNP_API_PKT RxApiPkt[RF_RING_BUF_NUM];
} gRxApiRingStruct={0};

CYFISNP_API_PKT *pRxApiPkt2; 		// ptr to FCD API data packet (Rx from Node)

// ---------------------------------------------------------------------------
//
// CYFISNP_RxReleaseRxdata() - Release Rx Packet buffer for reuse (MUST DO THIS)
//
// The radio ISR has a internal buffer: rxIsrBufPkt.  When the ISR receives a
//  packet and _RxApiPkt is free, then it copies rxIsrBufPkt to RxIsrBuf and
//  issue RxGo to the radio.  However if _RxApiPkt is occupied, the ISR holds
//  the Rx packet in its internal rxIsrBufPkt buffer, sets IsrRxOverflow
//  and DOES NOT issue RxGo (ie: it stops AutoAcking arriving packets).
//  So RxdataRelease() FIRST releases RxApiPkt, THEN checks for overflow and
//   recovers if overflow occurred.
//
// ---------------------------------------------------------------------------
void CYFISNP_RxdataRelease(void)
{
    // -----------------------------------------------------------------------
    // FIRST release the RxApiPkt buffer (Rx packet ISR may hit at any time)
    // -----------------------------------------------------------------------
    CYFISNP_RxApiPkt.type = CYFISNP_API_TYPE_NULL;
    CYFISNP_RxApiPkt.length = 0;

    // -----------------------------------------------------------------------
    // SECOND check for ISR overflow (ie: it has stopped receiving packets)
    // -----------------------------------------------------------------------
    if (CYFISNP_IsrRxOverflow) {
        // -------------------------------------------------------------------
        // Manually copy the ISR buffer to the RxIsrBuf
        // -------------------------------------------------------------------
//      CYFISNP_RxApiPkt.length = rxIsrBufLen;
//      CYFISNP_RxApiPkt.rssi = rxIsrBufRssi;
        uint8 *pSrc =     (uint8 *)    &rxIsrBufPkt;
        uint8 *pDst = (byte *)&CYFISNP_RxApiPkt;
        char len = *pSrc;
        for (len += 2; len != 0; --len) {
            *pDst++ = *pSrc++;
        }

        // -------------------------------------------------------------------
        // Release the overflow and enable the radio receiver
        // -------------------------------------------------------------------
        CYFISNP_IsrRxOverflow = 0;
        CYFISNP_phyRxGo();        
    }
}

void postAndRxGo(void)
{
    if( CYFISNP_RxApiPkt.length)
    {
        CYFISNP_ePhyState     = CYFISNP_PHY_SYNTH;
        CYFISNP_IsrRxOverflow = 1;                         
    }
    else
    {            
        postAndGoLen            = rxIsrBufPkt.rxIsrBufLen + 2;
        CYFISNP_RxApiPkt.length = rxIsrBufPkt.rxIsrBufLen;
        CYFISNP_RxApiPkt.rssi   = rxIsrBufPkt.rxIsrBufRssi;
        CYFISNP_RxApiPkt.type   = rxIsrBufPkt.rxIsrBufProt; 
        CYFISNP_RxApiPkt.devId  = rxIsrBufPkt.rxIsrBufDev;         
        for(ivar = 0;ivar < CYFISNP_FCD_PAYLOAD_MAX;ivar++)
            CYFISNP_RxApiPkt.payload[ivar] = rxIsrBufPkt.rxIsrBufPay[ivar]; 
    }
    CYFISNP_startReceive(); 
}

// ---------------------------------------------------------------------------
//
// CYFISNP_RxdataGet() - Return apiPkt->length != 0 if data pending
//                 If so, process data and call CYFISNP_RxdataRelease()
//
// ---------------------------------------------------------------------------
    CYFISNP_API_PKT *      // Ret ptr to an API packet
CYFISNP_RxdataGet(void)
{	
    if (CYFISNP_RxApiPkt.length >= 2) {
        CYFISNP_RxApiPkt.length -= 2;  // API payload length
    }
    return (&CYFISNP_RxApiPkt);
}
/*--------------------------------------------------------------------
;
; chkSeqBit - If required, check sequence bit to filter Rx duplicates
;
  -------------------------------------------------------------------*/
static word CRC_Error_count = 0;
void chkSeqBit(void)
{    
    if(CRC_Error_count > 0)
       CRC_Error_count--;
		
    if((rxIsrBufPkt.rxIsrBufProt & PKT_SEQBIT_MASK) == PKT_SEQSYNC)
		{
      postAndRxGo(); 
			return;
		}
    
    if((rxIsrBufPkt.rxIsrBufProt & PKT_SEQBIT_MASK) == PKT_SEQUNK)
		{
      CYFISNP_startReceive();
			return;
		}
    //if(rxIsrBufPkt.rxIsrBufDev == 2)
		//	CRC_Error_count = 0;
		
    ivar = rxIsrBufPkt.rxIsrBufProt & PKT_SEQBIT_MASK;
            
    bitMask = (0x01 << ( rxIsrBufPkt.rxIsrBufDev % 8));                                 // might have to change to 0x80        
              
    if((CYFISNP_af_BASE[rxIsrBufPkt.rxIsrBufDev/8 + afVsb_ofs] & bitMask) == 0)
    {
        CYFISNP_af_BASE[rxIsrBufPkt.rxIsrBufDev/8 + afVsb_ofs] ^= bitMask;
        if((ivar & 0xFF) == 0)
            CYFISNP_af_BASE[SB_BYTE_SZ + rxIsrBufPkt.rxIsrBufDev/8 ] |= bitMask;  
        postAndRxGo(); 
    }
    else
    {
    
        if((CYFISNP_af_BASE[rxIsrBufPkt.rxIsrBufDev/8 + afEsb_ofs] & bitMask))
        {                                       
            if ( ivar == 0 )
            {
                CYFISNP_startReceive();
            }
            else
            {
                CYFISNP_af_BASE[rxIsrBufPkt.rxIsrBufDev/8 + afEsb_ofs] ^= bitMask;
                postAndRxGo(); 
            }
        }
        else if ( ivar != 0 )
        {
            CYFISNP_startReceive();
        }
        else
        {
            CYFISNP_af_BASE[rxIsrBufPkt.rxIsrBufDev/8 + afEsb_ofs] ^= bitMask;
            postAndRxGo(); 
        }
    }
}

/*----------------------------------------------------------------------------
;
; CYFISNP_startReceive() - Reenable receiver with current Rx Buffer
;
; ----------------------------------------------------------------------------*/

void CYFISNP_startReceive(void)
{
    if(CYFISNP_ePhyState == CYFISNP_PHY_RXGO)
    {
        CYFISNP_SetPtr(&rxIsrBufPkt.rxIsrBufProt);
        CYFISNP_SetLength(CYFISNP_FCD_PAYLOAD_MAX + 2);
        CYFISNP_StartReceive();               
    }
}

/*----------------------------------------------------------------------------
;
; sendRspViaIsrBuf - Tx {UNBIND, ConRsp, ParmRsp} Packet in Node's BCD Window
; sendTxBcd        - Tx                    {data} Packet in Node's BCD Window
;
; Entry:
;    CYFISNP_Temp1 = tx packet byte length
; txIsrBuf[0] = Start of data packet to send
;
;  Exit:
;              Acc = radio status (AND A, CYFISNP_TXE_IRQ) tests for AutoAck
----------------------------------------------------------------------------*/
void sendRspViaIsrBuf(void)
{
    if( CYFISNP_ePhyState != CYFISNP_PHY_RXGO)
    {
        txRetVec = TX_RET_NONE;
    }
    else
    {
        
        //CyDelayUs(10 * bcdRspDly);
        if(bcdRspDly)
           Cy_DelayUS2(10*bcdRspDly);    //Cpu_Delay100US(bcdRspDly); 
        ivar = (CYFISNP_radioTxConfig & ~TX_DATMODE_MSK) | ((rxStatusVal & RX_DATMODE_MSK) << 3);
        CYFISNP_SetTxConfig(ivar);
        CYFISNP_RetryCount = 0;
        CYFISNP_StartTransmit(0, CYFISNP_Len);       
    }       
}


uint8 CYFISNP_IRQ_Read(void) 
{
    //return (PTGD_PTGD0 == 0x1);
    return IRQ_RF_STATUS();//(PTBD_PTBD5 == 0x1); 
}


/*******************************************************************************
* Function Name: CYFISNP_ISR_Interrupt
********************************************************************************
*
* Summary:
*   The default Interrupt Service Routine for CYFISNP_ISR.
*
*   Add custom code between the START and END comments to keep the next version
*   of this file from over-writing your code.
*
*   Note You may use either the default ISR by using this API, or you may define
*   your own separate ISR through ISR_StartEx().
*
* Parameters:  
*   None
*
* Return:
*   None
*
*******************************************************************************/
void CYFISNP_ISR_Interrupt(void)
{
    /*  Place your Interrupt code here. */
    /* `#START CYFISNP_ISR_Interrupt` */
    if(CYFISNP_IRQ_Read())
    {        
        if((CYFISNP_State & CYFISNP_TX) != 0 )
        {            
            txRetState = CYFISNP_GetTransmitState();
					  //CYFISNP_State = txRetState; 
            if( (txRetState & (TXC_IRQ | TXE_IRQ)) != 0)
            {
                if(txRetVec != 0)
                {
                    CYFISNP_EndTransmit(); 
                    CYFISNP_SetCrcSeed(((uint16)(CYFISNP_hubSeedMsb) << 8)|CYFISNP_hubSeedLsb);
                    if(txRetVec == TX_RET_UNBIND)
                    {
                        //txRetUnbind();
                        CYFISNP_startReceive();
                    }
                    else if(txRetVec == TX_RET_CONRSP)
                    {
                        // txRetConRsp();
                        /* --------------------------------------------------------------------
                        ; Valid Expected Sequence Bit = 1
                        ;       Expected Sequence Bit = 0
                        ;       Transmit Sequence Bit = 0
                        ----------------------------------------------------------------------*/    
                        CYFISNP_af_BASE[txIsrBuf[1]/8 + afVsb_ofs] |=  (0x01 << (txIsrBuf[1] % 8));
                        CYFISNP_af_BASE[txIsrBuf[1]/8 + afEsb_ofs] &= ~(0x01 << (txIsrBuf[1] % 8));
                        CYFISNP_af_BASE[txIsrBuf[1]/8 + afTsb_ofs] &= ~(0x01 << (txIsrBuf[1] % 8));
                        CYFISNP_startReceive();
                    }
                    else if(txRetVec == TX_RET_BINDRSP)
                    {
                        // txRetBindRsp();
                        if((txRetState & TXE_IRQ) != 0)
                        {
                            if(CYFISNP_BindProgress != PASSED_BIND_RSP)
                            {
                                CYFISNP_BindProgress = PASSED_BIND_RSP;
                                rxIsrBufPkt.rxIsrBufProt = PKT_BINDRSP_TYPE;
                                postAndRxGo();
                            }
                            else
                                CYFISNP_startReceive();
                        }
                        else
                        {
                            CYFISNP_BindProgress = PASSED_BIND_RSP_ACK;
                            rxIsrBufPkt.rxIsrBufProt = PKT_BINDRSPDEL_TYPE;
                            postAndRxGo();
                        }            
                    }
              #if CYFISNP_BCD_PAYLOAD_MAX
                    if(txRetVec == TX_RET_BCDRSP)
                    {
                        //txRetBcdRsp();
                        if((txRetState & TXE_IRQ) == 0)
                        {
                            /*------------------------------------------------------------
                            ; AutoAck received, clear this Device's BCD pending buffer
                            ; -----------------------------------------------------------*/
                            CYFISNP_BcdBufs[txIsrBuf[1] - 1].wprCntLen = 0;  //  Zero wprCntLen LENGTH field
                        }
                        chkSeqBit();   
                    }
              #endif      // CYFISNP_BCD_PAYLOAD_MAX
                }// No Return
            }// TX error       
        } // not in TX state
        else
        {
                
            if((CYFISNP_State & CYFISNP_RX) == 0) 
              return;
            
            CYFISNP_GetReceiveState();
            if((CYFISNP_State & (RXC_IRQ | RXE_IRQ)) !=0)
            {      
                if((CYFISNP_State & RXE_IRQ) != 0 ) // Complete
                {   
                  //rxeFlg();
                    CRC_Error_count++;                    
                    CYFISNP_EndReceive();
                    CYFISNP_Read(RX_STATUS_ADR);
                    CYFISNP_GetCrcSeed();
                    CYFISNP_startReceive();
                    
                    if(CRC_Error_count < 10) 
                    {                      
                      if(CYFISNP_ePhyState == CYFISNP_PHY_RXGO)
                      {   
                        CYFISNP_SetPtr(&rxIsrBufPkt.rxIsrBufProt);
                        CYFISNP_SetLength(CYFISNP_FCD_PAYLOAD_MAX + 2);
                        CYFISNP_StartReceive();               
                      }
                    } 
                    else 
                    {  
                      //LedRedOn();      Agust 29, Jason chen            
                      CRC_Error_count = 0;                       
             	        CYFISNP_Stop();
            	        CYFISNP_Start();            	      
                    }

                }
                else
                {   //LedRedOn();
                    rxIsrBufPkt.rxIsrBufLen       = CYFISNP_EndReceive();
                    rxStatusVal                   = CYFISNP_GetReceiveStatus();
                    rxIsrBufPkt.rxIsrBufRssi      = CYFISNP_GetRssi();
                    if((rxIsrBufPkt.rxIsrBufRssi & RSSI_LVL_MSK) == 0)
                        rxIsrBufPkt.rxIsrBufRssi  = CYFISNP_RssiPolled;
                    else
                        rxIsrBufPkt.rxIsrBufRssi &= RSSI_LVL_MSK;
                                                                               
                    if((rxStatusVal & RX_CRC0) != 0)
                    {
                        //zSeed();
                        if(((rxIsrBufPkt.rxIsrBufProt & PKT_BINDREQ_MASK) == PKT_BINDREQ_TYPE) && (rxIsrBufPkt.rxIsrBufLen == PKT_BIND_REQ_LEN))
                        {
                            char flag = 0;
                            //bind request
                            txIsrBuf[2] = rxIsrBufPkt.rxIsrBufPay[0];
                            txIsrBuf[3] = rxIsrBufPkt.rxIsrBufPay[1];
                            txIsrBuf[4] = rxIsrBufPkt.rxIsrBufPay[2];
                            txIsrBuf[5] = rxIsrBufPkt.rxIsrBufPay[3];
                            for(temp = 1 ; temp <= CYFISNP_MAX_NODES; temp++)
                            {
                                flag = 0;
                                for(ivar = 0 ; ivar < 4; ivar++)
                                {
                                    if(rxIsrBufPkt.rxIsrBufPay[ivar] != *(EEPROM_Pointer + GET_DEV_INDEX(temp) + DEV_REC_devMid_OFS + ivar))
                                    {
                                        break;
                                    }
                                }
                                if(ivar == 4)
                                {
                                    flag = 1;
                                    break;
                                }
                            }             
                            if(flag == 1)
                            {
                                txIsrBuf[0] = PKT_BINDRSP_TYPE;
                                txIsrBuf[1] = temp; // devid
                                rxIsrBufPkt.rxIsrBufDev = temp; // devid
                                txIsrBuf[6] = CYFISNP_hubSeedMsb;
                                txIsrBuf[7] = CYFISNP_hubSeedLsb;
                                txIsrBuf[8] = *(EEPROM_Pointer + 1);
                                txIsrBuf[9] = (*(EEPROM_Pointer) << 3) | *(EEPROM_Pointer + 2);
                                bcdRspDly = *(EEPROM_Pointer + GET_DEV_INDEX(temp) + DEV_REC_BCD_DLY_OFS);
                                CYFISNP_SetCrcSeed(((uint16)(txIsrBuf[2])<< 8) | txIsrBuf[3] );
                                txRetVec = TX_RET_BINDRSP;
                                CYFISNP_SetLength(PKT_BINDRSP_LEN);
                                CYFISNP_SetPtr(txIsrBuf);
                                sendRspViaIsrBuf(); 
                                return;
                            }
                            if(CYFISNP_BindProgress == PASSED_BIND_NONE)
                            {
                                CYFISNP_BindProgress = PASSED_BIND_REQ;
                                postAndRxGo();
                                return;
                            }
                            else
                            {
                                CYFISNP_startReceive();
                                return;
                            }
                        }
                        else
                            CYFISNP_startReceive();
                    }
                    else
                    {                                                          
                        if((rxIsrBufPkt.rxIsrBufProt & PKT_data_MASK) == PKT_data_TYPE)
                        {                                              
                            // rxdata();                                                                                         
                            if(rxIsrBufPkt.rxIsrBufDev)
                            {                             
                                devIdMatch = rxIsrBufPkt.rxIsrBufDev ^ (*(EEPROM_Pointer + GET_DEV_INDEX(rxIsrBufPkt.rxIsrBufDev) + DEV_REC_devId_OFS ));
                                if((rxIsrBufPkt.rxIsrBufProt & PKT_data_BCDR_MASK) != 0)
                                {
                                    //devIdMatch = rxIsrBufPkt.rxIsrBufDev ^ (*(EEPROM_Pointer + GET_DEV_INDEX(rxIsrBufPkt.rxIsrBufDev) + DEV_REC_devId_OFS ));
                                    bcdRspDly = *(EEPROM_Pointer + GET_DEV_INDEX(rxIsrBufPkt.rxIsrBufDev) + DEV_REC_BCD_DLY_OFS);
                                    if(devIdMatch)         // not exists!
                                    {
                                        CYFISNP_SetCrcSeed(((uint16)(CYFISNP_hubSeedMsb)<<8)|(CYFISNP_hubSeedLsb ^ rxIsrBufPkt.rxIsrBufDev));
                                        txIsrBuf[0] = PKT_UNBIND_TYPE;
                                        CYFISNP_SetLength(PKT_UNBIND_LEN);
                                        txRetVec = TX_RET_UNBIND;
                                        CYFISNP_SetPtr(txIsrBuf);
                                        sendRspViaIsrBuf();      
                                    }
                                    else
                                    {
#if CYFISNP_BCD_PAYLOAD_MAX & 0         // Even BCD enabled, don't reponse every packet received.
                                        #define WPR_LEN_MASK    0x3F
                                        #define dat_bcdDevId    txIsrBuf[2]
                                        #define dat_bcdPtrLsn   txIsrBuf[1]
                                        #define dat_bcdPktLen   txIsrBuf[0]
                                        dat_bcdDevId = rxIsrBufPkt.rxIsrBufDev;
                                        dat_bcdPktLen = CYFISNP_BcdBufs[rxIsrBufPkt.rxIsrBufDev - 1].wprCntLen & WPR_LEN_MASK;
                                        CYFISNP_SetCrcSeed(((uint16)(CYFISNP_hubSeedMsb)<<8)|(CYFISNP_hubSeedLsb ^ rxIsrBufPkt.rxIsrBufDev));
                                        CYFISNP_SetPtr(&CYFISNP_BcdBufs[rxIsrBufPkt.rxIsrBufDev - 1].bcd_data[0]);
                                        CYFISNP_SetLength(dat_bcdPktLen);
                                        txRetVec = TX_RET_BCDRSP;
                                        sendRspViaIsrBuf();
#elif 1
                                        txIsrBuf[0] = 0x30;
                                        txIsrBuf[1] = CYFISNP_hubSeedMsb;
                                        txIsrBuf[2] = CYFISNP_hubSeedLsb;
                                        txIsrBuf[3] = rxIsrBufPkt.rxIsrBufDev;//node 8
                                        
                                        CYFISNP_SetCrcSeed(((uint16)(CYFISNP_hubSeedMsb)<<8)|(CYFISNP_hubSeedLsb ^ rxIsrBufPkt.rxIsrBufDev));
                                        CYFISNP_SetLength(4);
                                        CYFISNP_SetPtr(txIsrBuf);
                                        txRetVec = TX_RET_BCDRSP;
                                        sendRspViaIsrBuf();
                                                                                
#endif  // CYFISNP_BCD_PAYLOAD_MAX

                                    }                                                                        
                                }
                                else                                  
                                    chkSeqBit();
                            }
                            else                                
                                CYFISNP_startReceive();
                        }
                        else if(rxIsrBufPkt.rxIsrBufProt == PKT_CONREQ_TYPE)
                        {
                            if(rxIsrBufPkt.rxIsrBufLen == PKT_CONREQ_LEN)
                            {
                                // conReq();                                
                                txIsrBuf[2] = rxIsrBufPkt.rxIsrBufPay[0];
                                txIsrBuf[3] = rxIsrBufPkt.rxIsrBufPay[1];
                                txIsrBuf[4] = rxIsrBufPkt.rxIsrBufPay[2];
                                txIsrBuf[5] = rxIsrBufPkt.rxIsrBufPay[3];
                                for(ivar = 0 ; ivar < 4; ivar++)
                                {
                                    if(rxIsrBufPkt.rxIsrBufPay[ivar] != *(EEPROM_Pointer + GET_DEV_INDEX(rxIsrBufPkt.rxIsrBufDev) + DEV_REC_devMid_OFS + ivar))
                                    {                                          
                                          txIsrBuf[1] = 0;  // MID not in flash   for Node side unbind process
                                          break;
                                    }
                                }
                                if(ivar == 4)
                                    txIsrBuf[1] = rxIsrBufPkt.rxIsrBufDev;  // devid
                                txIsrBuf[0] = PKT_CONRSP_TYPE;
                                bcdRspDly = 2;//*(EEPROM_Pointer + GET_DEV_INDEX(rxIsrBufPkt.rxIsrBufDev) + DEV_REC_BCD_DLY_OFS );
                                CYFISNP_SetCrcSeed(((uint16)(CYFISNP_hubSeedMsb)<< 8) | (CYFISNP_hubSeedLsb ^ rxIsrBufPkt.rxIsrBufDev));
                                txRetVec = TX_RET_CONRSP;
                                CYFISNP_SetLength(PKT_CONRSP_LEN);
                                CYFISNP_SetPtr(txIsrBuf); 
                           
                                if(txIsrBuf[1] == 0) 
                                {
                                #if 1
                                   Cy_DelayUS2(500);               //Cpu_Delay100US(5);    
                                   ivar = (CYFISNP_radioTxConfig & ~TX_DATMODE_MSK) | ((rxStatusVal & RX_DATMODE_MSK) << 3);
                                   CYFISNP_SetTxConfig(ivar);
                                   CYFISNP_StartTransmit(0, PKT_CONRSP_LEN);                                                                                                   
                                #endif
                                } 
                                else                          
                                  sendRspViaIsrBuf();                                                                
                            }
                            else
                            {
                                postAndRxGo();
                            }
                        }
                        else
                        {
                            //rxUnkPkt();
                            rxIsrBufPkt.rxIsrBufProt = PKT_UNKNOWN_TYPE;
                            postAndRxGo();
                        }
                    }
                }
            }
        } //RX State
    }// pin not set
    /* `#END` */
}





/* [] END OF FILE */
