//*****************************************************************************
//*****************************************************************************
//  FILENAME: cyfi_hal.c
//  Version: 0.99, Updated on 20/07/2013

//
//  DESCRIPTION: <HUB>Star Network Protocol Hardware abstraction layer
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//******************************************************************************
//******************************************************************************

#include "cyfi_hal.h"
//#include "CYFISNP_SPIM.h"
//#include "CYFISNP_IRQ.h"
//#include "derivative.h"              /* include peripheral declarations */

#include "gpio.h"
#include "spi.h"


uint8        			*CYFISNP_Ptr; /* Ptr to User's Payload buffer */
uint8           	CYFISNP_Len;  /* Len of User's Payload buffer */
uint8           	fInterActive;

#define SPI_ADDRESS                (uint8)0x3F /* Address field mask */
#define SPI_WRITE                  (uint8)0x80 /* SPI write to CYFI follows */
#define SPI_AUTO_INC               (uint8)0x40 /* Auto increment CYFI adr register */

//#pragma MESSAGE DISABLE C1420


/*******************************************************************************
 * Function Name:  void CYFISNP_Reset(void)
 *******************************************************************************
 * Summary:
 * Send Soft Reset command to Radio.
 ******************************************************************************/
void CYFISNP_Reset(void)
{
    CYFISNP_Deselect();
    CYFISNP_Write(MODE_OVERRIDE_ADR, RST);
}

/*******************************************************************************
 * Function Name: void CYFISNP_Write(uint8 addr, uint8 data)
 *******************************************************************************
 * Summary:
 * Writes data to the specified register address.
 ******************************************************************************/
uint8_t spiValue;
void CYFISNP_Write(uint8 addr, uint8 data)
{
    CYFISNP_Select();
	  spiValue = addr|SPI_WRITE;
    CYFISNP_ByteWrite(spiValue);
    CYFISNP_ByteWrite(data);
    CYFISNP_Deselect();
}

/*******************************************************************************
 * Function Name: uint8 CYFISNP_ByteWrite(uint8 byte)
 *******************************************************************************
 * Summary:
 * Puts a byte on the SPI bus and waits for a response.
 ******************************************************************************/
uint8 CYFISNP_ByteWrite(uint8 data)
{
	 uint8_t recvData = CYFISNP_SPI_ByteWrite(data);
   return recvData;	
}

/*******************************************************************************
 * Function Name: uint8 CYFISNP_Read(uint8 addr)
 *******************************************************************************
 * Summary:
 * Reads and returns data from the specified register address.
 ******************************************************************************/
uint8  CYFISNP_Read(uint8 adr)
{
    uint8  retVal;
    CYFISNP_Select();
    CYFISNP_ByteWrite(adr); /* Write CYFI register address */                
    retVal = CYFISNP_ByteWrite(0x0); /*  Read  CYFI register contents */
    CYFISNP_Deselect();
    return retVal;
}

/*******************************************************************************
 * Function Name: uint8  CYFISNP_ReadStatusDebounced(uint8 adr)
 *******************************************************************************
 * Summary:
 * Safe reading from RX_IRQ_STATUS_ADR. Read RX_IRQ_STATUS_ADR and debounce the
 * update of RXC/RXE bits. If only one of those two bits is set, read again and
 * OR the results. The 2nd read happens in the same SPI transaction.
 ******************************************************************************/
uint8  CYFISNP_ReadStatusDebounced(uint8 adr)
{
    uint8  retVal;

    retVal = CYFISNP_Read(adr);    
    /* If COMPLETE and ERROR bits mismatch, then re-read register
     * If RXC and RXE (or TXC and TXE) bits match, then skip the debounce read
     */
    if (((retVal & (RXC_IRQ | RXE_IRQ)) != 0) &&
        ((retVal & (RXC_IRQ | RXE_IRQ)) != (RXC_IRQ | RXE_IRQ))) {
            /* re-read and make bits sticky */
            retVal |= CYFISNP_Read(adr);
    }
    return retVal;
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetPtr(uint8 *ramPtr)
 *******************************************************************************
 * Summary:
 * Set the buffer pointer.
 ******************************************************************************/
void CYFISNP_SetPtr(uint8 * ramPtr)
{
    CYFISNP_Ptr = ramPtr;
}

/*******************************************************************************
 * Function Name: void CYFISNP_SetLength(uint8 length)
 *******************************************************************************
 * Summary:
 * Set the buffer length for CYFISNP_BurstRead & CYFISNP_FileRead
 ******************************************************************************/
void CYFISNP_SetLength(uint8 length)
{
    CYFISNP_Len = length;
}

/*******************************************************************************
 * Function Name: void CYFISNP_BurstWrite(uint8 adr, uint8 cnt)
 *******************************************************************************
 * Summary:
 * Write a sequence of bytes to a sequence of CYFISNP_ registers
 * Must call CYFISNP_SetPtr() prior to CYFISNP_BurstWrite().
 ******************************************************************************/
void CYFISNP_BurstWrite(uint8 adr, uint8 cnt)
{
    CYFISNP_FileWrite(adr | SPI_AUTO_INC, cnt);
}

/*******************************************************************************
 * Function Name: void CYFISNP_FileWrite(uint8 adr, uint8 cnt)
 *******************************************************************************
 * Summary:
 * Write a sequence of bytes to the same CYFISNP_ register.
 * Must call CYFISNP_SetPtr() prior to CYFISNP_BurstWrite().
 ******************************************************************************/
void CYFISNP_FileWrite(uint8 adr, uint8 cnt)
{
    uint8  *ptr;
    if (cnt != 0) {
            CYFISNP_Select();
            CYFISNP_ByteWrite(adr | SPI_WRITE); /*  Address to be writing */                
            ptr = CYFISNP_Ptr;
            do {   
                    /* Write a data Byte */
                    CYFISNP_ByteWrite(*ptr);                        
					ptr++;
					cnt--;
            } while (cnt != 0);
            CYFISNP_Deselect();
    }   
}

/*******************************************************************************
 * Function Name: void CYFISNP_BurstRead(uint8 adr, uint8 cnt)
 *******************************************************************************
 * Summary:
 * Read a sequence of bytes from sequence of CYFISNP_ registers
 ******************************************************************************/
void CYFISNP_BurstRead(uint8 adr, uint8 cnt)
{
    CYFISNP_FileRead(adr | SPI_AUTO_INC, cnt);
}


/*******************************************************************************
 * Function Name: void CYFISNP_FileRead(uint8 adr, uint8 storeCt)
 *******************************************************************************
 * Summary:
 * Read a sequence of bytes from a single CYFISNP_ register. 
 * This routine properly handles all cases of length including zero and one. 
 * CYFISNP_Ptr and CYFISNP_Len must be set prior to calling CYFISNP_FileRead()
 ******************************************************************************/
void CYFISNP_FileRead(uint8 adr, uint8 storeCt)
{
    uint8 * ptr,i;
    uint8  dumpCt = 0; /* Bytes to read and discard  */
    ptr = CYFISNP_Ptr;
    dumpCt = 0;
//debug purpose only start
for(i = 0; i<storeCt; i++)
    ptr[0] = 0;
//debug purpose only end

    if (storeCt != 0) {
            CYFISNP_Select();
            CYFISNP_ByteWrite(adr); /* Address(es) to read from */                
            if (storeCt > CYFISNP_Len) {
                    dumpCt = storeCt - CYFISNP_Len;   
                    storeCt = CYFISNP_Len;
            }

            while (storeCt > 0) {                       
                   *ptr = CYFISNP_ByteWrite(0x0);   /* Read/store CYFISNP_ Byte */
                   --storeCt;
                   ++ptr;
            }

            while (dumpCt > 0) {                       
                   (void)CYFISNP_ByteWrite(0x0); /* Discard the extra buffer */
                   --dumpCt;
            }
            CYFISNP_Deselect();
    }
}

/*******************************************************************************
 * Function Name: uint8 IsCYFISNP_InterruptActive(void)
 *******************************************************************************
 * Summary:
 * Rerturns value on IRQ pin
 ******************************************************************************/
uint8  IsCYFISNP_InterruptActive(void)
{
    //return(CYFISNP_IRQ_Read());  
    //return (PTGD_PTGD0 == 0x1);
    return IRQ_RF_STATUS();//(PTBD_PTBD5 == 0x1);   
}

