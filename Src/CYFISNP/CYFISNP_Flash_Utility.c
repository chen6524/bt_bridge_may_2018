//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Flash_Utility.c
//  Version:0.99 , Updated on 29/08/2017
//
//  DESCRIPTION: <HUB> CYFISNP Protocol Net/device Store
//-----------------------------------------------------------------------------
//  Copyright (c) Artaflex INC  2017. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include <string.h>
#include "stm32l4xx_hal.h"
#include "CYFISNP_Flash_Utility.h"
#include "tim.h"
#include "iwdg.h"
//#include "CYFISNP_Config.h"
//#include "CYFISNP_Eeprom.h"

/* Private Node table typedef -----------------------------------------------------------*/
NODE_TBL nodeTable1;
const NODE_TBL nodeTableFlash1 __attribute__((at(ADDR_FLASH_PAGE_32)));
NODE_TBL nodeTable2;
const NODE_TBL nodeTableFlash2 __attribute__((at(ADDR_FLASH_PAGE_33)));

NODE_TBL *pNodeTable[2];
NODE_TBL *pNodeTableFlash[2];

//#define PROFILE_TABLE0_START_ADDR   ADDR_FLASH_PAGE_32
//#define PROFILE_TABLE1_START_ADDR   ADDR_FLASH_PAGE_33
//#define PROFILE_TABLE0_END_ADDR     PROFILE_TABLE0_START_ADDR + FLASH_PAGE_SIZE - 1
//#define PROFILE_TABLE1_END_ADDR     PROFILE_TABLE1_START_ADDR + FLASH_PAGE_SIZE - 1

/* Private define ------------------------------------------------------------*/
#define FLASH_USER_START_ADDR   ((uint32_t)NET_PARAMETER_ADDR)                             /* Start @ of user Flash area */   //((uint32_t)0x0803F800)
#define FLASH_USER_END_ADDR     FLASH_USER_START_ADDR + FLASH_PAGE_SIZE - 1       /* End @ of user Flash area */

uint32_t DATA_32;                // ((uint32_t)0x12345678)
uint64_t DATA_64;                // ((uint64_t)0x1234567812345678)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t FirstPage = 0, NbOfPages = 0, BankNumber = 0;
uint32_t Address   = 0, PAGEError = 0;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

//uint32_t WriteBufferAddr = 0;

/*Variable used for Erase procedure*/
static FLASH_EraseInitTypeDef EraseInitStruct;

/* Private function prototypes -----------------------------------------------*/
static uint32_t GetPage(uint32_t Address);
static uint32_t GetBank(uint32_t Address);

uint16_t i;
uint16_t length = 0;
uint8_t Table_Program(uint8_t *WriteBuffer, uint16_t len, uint32_t table_addr)
{
	//uint16_t length = 0;
	
	length = len / 8;
	length = length * 8;
	
	
	if(length < len)
	{
		length +=8;//= len/8 + 8;
	}
	DisableInterrupts;
  /* Unlock the Flash to enable the flash control register access *************/
  HAL_FLASH_Unlock();

  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  /* Clear OPTVERR bit set on virgin samples */
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR); 
  /* Get the 1st page to erase */
  FirstPage = GetPage(table_addr);
  /* Get the number of pages to erase from 1st page */
  NbOfPages = GetPage(table_addr + FLASH_PAGE_SIZE - 1) - FirstPage + 1;
  /* Get the bank */
  BankNumber = GetBank(table_addr);
  /* Fill EraseInit structure*/
  EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.Banks       = BankNumber;
  EraseInitStruct.Page        = FirstPage;
  EraseInitStruct.NbPages     = NbOfPages;
	
  /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
     you have to make sure that these data are rewritten before they are accessed during code
     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
     DCRST and ICRST bits in the FLASH_CR register. */
		 while(1)
		 {
				if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
				{
					/*
						Error occurred while page erase.
						User can add here some code to deal with this error.
						PAGEError will contain the faulty page and then to know the code error on this page,
						user can call function 'HAL_FLASH_GetError()'
					*/
					/* Infinite loop */
					PAGEError = HAL_FLASH_GetError();
					//while (1)
					{
						/* Make LED5 blink (1s period) to indicate error in Erase operation */
						//__enable_irq();
						BLU_LED_On();
						//HAL_Delay(1000);
						//RED_LED_Off();
						//HAL_Delay(1000);
					}
				}
				else
					break;
     }	
	   BLU_LED_Off();
  /* Program the user Flash area word by word
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  Address         = table_addr;
	i = 0;

  //while (Address < FLASH_USER_END_ADDR)
  while ( i < length)
  {
		DATA_64 = *(__IO uint64_t *)(&WriteBuffer[i]);
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, Address, DATA_64) == HAL_OK)
    {
      Address         = Address + 8;
			i = i + 8;
    }
   else
    {
      /* Error occurred while writing data in Flash memory.
         User can add here some code to deal with this error */
      while (1)
      {
        /* Make LED5 blink (1s period) to indicate error in Write operation */
        RED_LED_On();
        HAL_Delay(1000);
        RED_LED_Off();
        HAL_Delay(1000);
      }
    }
  }

  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();

  /* Check if the programmed data is OK
      MemoryProgramStatus = 0: data programmed correctly
      MemoryProgramStatus != 0: number of words not programmed correctly ******/
  Address = table_addr;
  MemoryProgramStatus = 0x0;
	i = 0;
  //while (Address < FLASH_USER_END_ADDR)
	while (i < length)
  {
    data32  = *(__IO uint32_t *)Address;
		DATA_32 = *(__IO uint32_t *)(&WriteBuffer[i]);
    if (data32 != DATA_32)
    {
      MemoryProgramStatus++;
    }
    Address         = Address + 4;
		i               = i + 4;
  }

  /*Check if there is an issue to program data*/
  if (MemoryProgramStatus == 0)
  {
    /* No error detected. Switch on LED4*/
    //GRN_LED_On();
		EnableInterrupts;		
		return 0;
  }
  else
  {
    /* Error detected. Switch on LED5*/
    BLU_LED_On();
		EnableInterrupts;		
		return 1;
  }
}

uint16_t tbl_size = 0;
void Node_Table_Init(void)
{
  uint16_t i;
	CYFISNP_EEP_DEV_REC const *pDevNet;// = CYFISNP_EEP_DEV_REC_ADR;		
	
	pNodeTableFlash[0] = (NODE_TBL *)(&nodeTableFlash1.magic);
	pNodeTableFlash[1] = (NODE_TBL *)(&nodeTableFlash2.magic);
	
	pNodeTable[0]      = (NODE_TBL*)(&nodeTable1.magic);
	pNodeTable[1]      = (NODE_TBL*)(&nodeTable2.magic);
	
	tbl_size = TABLE_SIZE;
	//size_t
	(void)memcpy((void*)pNodeTable[0],(void*)pNodeTableFlash[0], tbl_size);	
	(void)memcpy((void*)pNodeTable[1],(void*)pNodeTableFlash[1], tbl_size);	
	if((pNodeTable[0]->magic != MAGIC_SIGNATURE)||(pNodeTable[1]->magic != MAGIC_SIGNATURE))
	{
	  //uint8_t const *pFlash = (uint8_t const*)(&CYFISNP_EEP_DEV_REC_ADR[devId]);		
		pNodeTable[0]->magic = MAGIC_SIGNATURE;
		pNodeTable[1]->magic = MAGIC_SIGNATURE;
		for(i = 0;i < CYFISNP_MAX_NODES;i++)
		{	
			__RESET_WATCHDOG();                                                            //MX_IWDG_RESET();
      pDevNet = &CYFISNP_EEP_DEV_REC_ADR[i+1];			
			if(pDevNet->devId)
			{
				if(i < HALF_NODE_SIZE)
				{
					(void)memset((void*)pNodeTable[0]->nodeProfile[i].GID, 0xEE, 6);
					pNodeTable[0]->nodeProfile[i].playerNo = i+1;
					(void)memcpy((void*)pNodeTable[0]->nodeProfile[i].name, "GFT", 3);
					{
						pNodeTable[0]->nodeProfile[i].name[3] = 0x30;
						pNodeTable[0]->nodeProfile[i].name[4] = 0x30 + (i+1)/10;
						pNodeTable[0]->nodeProfile[i].name[5] = 0x30 + (i+1)-(i+1)/10;
					}
				}			
				else
				{
					(void)memset((void*)pNodeTable[1]->nodeProfile[i - HALF_NODE_SIZE].GID, 0xDD, 6);
					pNodeTable[1]->nodeProfile[i - HALF_NODE_SIZE].playerNo = i+1;
					(void)memcpy((void*)pNodeTable[1]->nodeProfile[i - HALF_NODE_SIZE].name, "GFT", 3);
					{
						if( i < 99)
						{
							pNodeTable[1]->nodeProfile[i].name[3] = 0x30;
							pNodeTable[1]->nodeProfile[i].name[4] = 0x30 + (i+1)/10;
							pNodeTable[1]->nodeProfile[i].name[5] = 0x30 + (i+1)-(i+1)/10;
						}
						else
						{
							pNodeTable[1]->nodeProfile[i].name[3] = 0x31;
							pNodeTable[1]->nodeProfile[i].name[4] = 0x30 + (100 - i - 1)/10;
							pNodeTable[1]->nodeProfile[i].name[5] = 0x30 + (100 - i - 1)-(100 - i - 1)/10;						
						}
					}				
				}
		  }
			else
			{
				if(i < HALF_NODE_SIZE)
					(void)memset((void*)pNodeTable[0]->nodeProfile[i].GID, 0 , NODE_PROFILE_SIZE);	
				else
					(void)memset((void*)pNodeTable[1]->nodeProfile[i].GID, 0 , NODE_PROFILE_SIZE);	
			}
		}		
		Table_Program((uint8_t*)pNodeTable[0], tbl_size, PROFILE_TABLE0_START_ADDR);
		Table_Program((uint8_t*)pNodeTable[1], tbl_size, PROFILE_TABLE1_START_ADDR);	
	}
}



uint8_t Flash_Program(uint8_t *WriteBuffer, uint16_t len)
{
	uint16_t i, length = 0;
	
	length = len / 8;
	length = length * 8;
	
	
	if(length < len)
	{
		length +=8;//= len/8 + 8;
	}
  /* Unlock the Flash to enable the flash control register access *************/
  HAL_FLASH_Unlock();

  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  /* Clear OPTVERR bit set on virgin samples */
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR); 
  /* Get the 1st page to erase */
  FirstPage = GetPage(FLASH_USER_START_ADDR);
  /* Get the number of pages to erase from 1st page */
  NbOfPages = GetPage(FLASH_USER_END_ADDR) - FirstPage + 1;
  /* Get the bank */
  BankNumber = GetBank(FLASH_USER_START_ADDR);
  /* Fill EraseInit structure*/
  EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.Banks       = BankNumber;
  EraseInitStruct.Page        = FirstPage;
  EraseInitStruct.NbPages     = NbOfPages;
	
  /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
     you have to make sure that these data are rewritten before they are accessed during code
     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
     DCRST and ICRST bits in the FLASH_CR register. */
		 while(1)
		 {
				if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
				{
					/*
						Error occurred while page erase.
						User can add here some code to deal with this error.
						PAGEError will contain the faulty page and then to know the code error on this page,
						user can call function 'HAL_FLASH_GetError()'
					*/
					/* Infinite loop */
					PAGEError = HAL_FLASH_GetError();
					//while (1)
					{
						/* Make LED5 blink (1s period) to indicate error in Erase operation */
						//__enable_irq();
						BLU_LED_On();
						//HAL_Delay(1000);
						//RED_LED_Off();
						//HAL_Delay(1000);
					}
				}
				else
					break;
     }	
	   BLU_LED_Off();
  /* Program the user Flash area word by word
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  Address         = FLASH_USER_START_ADDR;
	i = 0;

  //while (Address < FLASH_USER_END_ADDR)
  while ( i < length)
  {
		DATA_64 = *(__IO uint64_t *)(&WriteBuffer[i]);
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, Address, DATA_64) == HAL_OK)
    {
      Address         = Address + 8;
			i = i + 8;
    }
   else
    {
      /* Error occurred while writing data in Flash memory.
         User can add here some code to deal with this error */
      while (1)
      {
        /* Make LED5 blink (1s period) to indicate error in Write operation */
        RED_LED_On();
        HAL_Delay(1000);
        RED_LED_Off();
        HAL_Delay(1000);
      }
    }
  }

  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();

  /* Check if the programmed data is OK
      MemoryProgramStatus = 0: data programmed correctly
      MemoryProgramStatus != 0: number of words not programmed correctly ******/
  Address = FLASH_USER_START_ADDR;
  MemoryProgramStatus = 0x0;
	i = 0;
  //while (Address < FLASH_USER_END_ADDR)
	while (i < length)
  {
    data32  = *(__IO uint32_t *)Address;
		DATA_32 = *(__IO uint32_t *)(&WriteBuffer[i]);
    if (data32 != DATA_32)
    {
      MemoryProgramStatus++;
    }
    Address         = Address + 4;
		i               = i + 4;
  }

  /*Check if there is an issue to program data*/
  if (MemoryProgramStatus == 0)
  {
    /* No error detected. Switch on LED4*/
    //GRN_LED_On();
		return 0;
  }
  else
  {
    /* Error detected. Switch on LED5*/
    BLU_LED_On();
		return 1;
  }
}

/**
  * @brief  Gets the page of a given address
  * @param  Addr: Address of the FLASH Memory
  * @retval The page of a given address
  */
static uint32_t GetPage(uint32_t Addr)
{
  uint32_t page = 0;
  
  if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
  {
    /* Bank 1 */
    page = (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
  }
  else
  {
    /* Bank 2 */
    page = (Addr - (FLASH_BASE + FLASH_BANK_SIZE)) / FLASH_PAGE_SIZE;
  }
  
  return page;
}

/**
  * @brief  Gets the bank of a given address
  * @param  Addr: Address of the FLASH Memory
  * @retval The bank of a given address
  */
static uint32_t GetBank(uint32_t Addr)
{
  uint32_t bank = 0;
#if 0  
  if (READ_BIT(SYSCFG->MEMRMP, SYSCFG_MEMRMP_FB_MODE) == 0)
  {
  	/* No Bank swap */
    if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
    {
      bank = FLASH_BANK_1;
    }
    else
    {
      bank = FLASH_BANK_2;
    }
  }
  else
  {
  	/* Bank swap */
    if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
    {
      bank = FLASH_BANK_2;
    }
    else
    {
      bank = FLASH_BANK_1;
    }
  }
#endif  
  return bank;
}

